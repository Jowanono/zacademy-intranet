import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable()
export class PeopleListService {
  constructor(private http: HttpClient) {}

  getAllStudents(): Observable<string[]> {
    return this.http.get<string[]>("http://localhost:8080/promotions/rennes-03/students");
  }
}
