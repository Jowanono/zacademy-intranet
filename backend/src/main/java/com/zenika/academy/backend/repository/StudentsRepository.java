package com.zenika.academy.backend.repository;

import com.zenika.academy.backend.domain.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface StudentsRepository extends CrudRepository<Student, String> {
    List<Student> findAll();
}
