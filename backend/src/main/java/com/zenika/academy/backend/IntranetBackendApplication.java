package com.zenika.academy.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
public class IntranetBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(IntranetBackendApplication.class, args);
	}

}
